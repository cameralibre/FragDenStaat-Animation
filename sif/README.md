[/sif](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/sif)  
[Synfig file format](http://wiki.synfig.org/wiki/File_Formats)  
The main animation files are [Synfig](http://www.synfig.org/) projects (.sif). There's one project for each scene in the video.   
Other than some linked texture files, the elements in the projects are all vector graphics, meaning they are simply mathematical expressions of points, lines, curves, shapes or polygons. 
This means that the projects are very flexible, lightweight*, and as the projects are text-based XML files, they lead themselves well to version control & scripting. 

*ok, well, lightweight compared to raster images - any kind of animation is going to be a lot of information. For example, scene 03 Post is only 8 seconds long, but it's still 22MB worth of text (almost 450000 lines).

To learn the basics of Synfig, I would recommend the official [Synfig Studio Training Course](https://gumroad.com/l/TkoN).