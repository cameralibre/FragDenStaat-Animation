[/fonts](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/fonts)  
A large part of adapting the project for international use is replacing German-language text with the local language. For this, you can edit and replace the text in each scene's Synfig .sif project.  
The FdS website uses [Kreon](https://www.google.com/fonts/specimen/Kreon) as its main font, hence its use here, though you may want to choose another font to fit your site's branding and design.  
KiMitTud uses [Roboto Slab](https://fonts.google.com/specimen/Roboto+Slab), which replaces Kreon in the hungarian version.
[Pecita](http://pecita.eu/) and [NotCourierSans](http://ospublish.constantvzw.org/foundry/notcouriersans/) have been used for the addresses on the envelopes in scene '[_03 Post_](https://gitlab.com/cameralibre/FragDenStaat-Animation/blob/master/sif/)'.  
[eufm10](http://www.ams.org/publications/authors/tex/amsfonts), [League Gothic](https://www.theleagueofmoveabletype.com/league-gothic), and [Cardo](http://www.scholarsfonts.net/cardofnt.html) are used for the newspaper in scene '[_09 Combining Data_](https://gitlab.com/cameralibre/FragDenStaat-Animation/blob/master/sif/)'.
CourierPrime is used for the legal gobbledygook revealed in scene [_11 browser_](https://gitlab.com/cameralibre/FragDenStaat-Animation/blob/master/sif/)'.  

Text layers in Synfig files can be modified by using the 'Find & Replace' function in a text editor, though different word lengths are likely to affect layout, so you may have to open the project in Synfig and adjust things manually to ensure everything fits.
