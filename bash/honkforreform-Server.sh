HONK=$1
FOR=$2
REFORM=$3
cp hupen.sif ./$HONK$FOR$REFORM.sif
sed -i -e "s/HUPEN/$HONK/g" ./$HONK$FOR$REFORM.sif
sed -i -e "s/FÜR/$FOR/g" ./$HONK$FOR$REFORM.sif
sed -i -e "s/REFORMEN/$REFORM/g" ./$HONK$FOR$REFORM.sif
echo "rendering animation as PNG sequence"
synfig ./$HONK$FOR$REFORM.sif -o $HONK$FOR$REFORM.png
echo "creating video"
ffmpeg -i $HONK$FOR$REFORM.%04d.png -vcodec png -hide_banner -loglevel 0 $HONK$FOR$REFORM.mov
echo "cleaning up"
rm *.png
