## Scene 09: combining data.sif

## Voiceover:

"information distributed throughout different parts of public administration can be combined to uncover interesting stories."

## Scene Description:

![combining data](http://www.cameralibre.cc/wp-content/uploads/combining_data-300x169.jpg)

1. we see three transparency sheets in the reporter's hands - the data points look chaotic. As she slides them together, a pattern forms between them.

![newspaper](http://www.cameralibre.cc/wp-content/uploads/newspaper-300x169.jpg)

2. this pattern is shown in a newspaper article announcing a scandal, or a big news story.

## Changes for international contexts:  
Name of the newspaper
Headline
Subheading

Install the included fonts in your system and change these headings in the Inkscape .svg file (/svg/09 combining data.svg) before saving as a Synfig .sif. You can then copy and paste the text as a group, between Synfig projects.
