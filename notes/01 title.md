## Scene 01: Title.sif

## Scene Description:

![title](http://www.cameralibre.cc/wp-content/uploads/01_title-300x169.jpg)

1. title and logo is displayed, and fades out

## NOTES

I initially animated the title on, and then decided to just display a static title. the animation properties are still available in the synfig file if you prefer to animate your title.

## Changes for international contexts:  

- Logo, title, and name of website all need to be changed, and likely the font as well.
