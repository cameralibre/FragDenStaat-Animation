## Scene 06: document handover.sif

## Voiceover: 

"with a Freedom of Information request, you can use facts to support your cause."

## Scene Description:

  
from the bottom left, a protestor places their FOI request onto a desk.  

![document handover](http://www.cameralibre.cc/wp-content/uploads/document_handover-300x169.jpg)

In response, an official places a binder of documents onto the desk, entering from the top of the frame.

The protestor takes the binders out of frame at bottom right.

## NOTES:  
The protestor's left and right hands are both the same group of shapes, just flipped over the 0 point on the horizontal axis: 

There's a waypoint at 00:00:02:06 where the 'protestor arm' group's 'Scale' value is inverted on the horizontal axis, going from (-59,1102px, 59,1102px) to (-59,1102px, 59,1102px). The same transformation happens to the 'protestor thumb' group.


## Changes for international contexts:  
None, unless changing the video's colour scheme.
