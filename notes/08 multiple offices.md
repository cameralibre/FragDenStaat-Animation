## Scene 08: multiple offices.sif

## Voiceover:

"information distributed throughout different parts of public administration can be combined to uncover interesting stories."

## Scene Description:

A reporter stands in front of an official's desk (green), and hands over an FOI request - the official searches for the information

The office slides out of view and the reporter is left with a folder under her arm as a new office (blue), with a new official, slides into view.

![multiple offices](http://www.cameralibre.cc/wp-content/uploads/multiple_offices-300x169.jpg)

The second official finds the requested information and a second folder appears under the reporter's arm.

A third office (yellow), with a third official, slides into frame, and we see the official present another folder of information to the reporter.

## Changes for international contexts:  

None necessary, only if colours/branding changes are required.

