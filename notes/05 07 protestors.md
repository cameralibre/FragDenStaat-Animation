## Scene 05 07 prostestors.sif

### Voiceover: 

_"without information it can sometimes be difficult to be heard. With a Freedom of Information request, you can support your cause with evidence."_ 

### Scene Description:

This Synfig file actually contains both scene 5 and scene 7.
Scene 05:   

three lonely protestors try to get attention for their cause. One has a sign saying 'honk for reform'

![sad protestors](http://www.cameralibre.cc/wp-content/uploads/05_protestors-300x169.jpg)
 
a truck drives by, ignoring them and covering them in smoke. the protestors are disheartened.

Scene 07:   

![happy protestors](http://www.cameralibre.cc/wp-content/uploads/07_protestors-300x169.jpg)

now the protestors have crowd of supporters and press coverage

they are greeted with friendly beeps from a passing car

### NOTES:  


### Changes for international contexts:  
text for the following placards will need to be translated and replaced:
frontwomanblue/no thanks
1frontmanblackC/honk for reform
we've had enough

