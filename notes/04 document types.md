## Scene 04 document types.sif

### Voiceover: 

_"through the Freedom of Information Act, you can request various types of documents - for example: building plans, correspondence, data sets, or internal meeting records."_ 

### Scene Description:

  
a paper with a question mark on it is pushed onto screen (representing an FOI request)

![document types](http://www.cameralibre.cc/wp-content/uploads/document_types-300x169.jpg)

different types of documents fall past the 'camera' as they are mentioned, and land on the ground.

### NOTES:  
Each paper is a flat rectangular plane created in inkscape, which is animated using [Rotate](http://wiki.synfig.org/wiki/Rotate_Layer), [Scale](http://wiki.synfig.org/wiki/Scale_Layer) and [Translate](http://wiki.synfig.org/wiki/Translate_Layer) layers in Synfig. The FOI request paper also has a small amount of animation of its upper corners, to show the paper flapping against the air as it is pushed forward. Each paper has a paper texture applied to it, and a shadow, animated to create a sense of the ground plane.


### Changes for international contexts:  
Just colours if using a different colour scheme.
