## Scene 02: inside government.sif

## Voiceover:

"With the Freedom of Information Act, citizens can receive information from authorities - these authorities publish a lot of important information, but by no means everything!
  
the Freedom of Information Act allows us to look into unpublished files and documents - when you use the FOI, you make the work of officials more public and transparent."

## Scene Description:

![FOI](http://www.cameralibre.cc/wp-content/uploads/02_inside_government.0039-300x169.png)

the FOI Act is signed. 

![rathaus](http://www.cameralibre.cc/wp-content/uploads/rathaus-300x169.jpg)

the Act is removed and we see a government building. We zoom in and enter the building to see an official at an information desk, lit by a small light.

![official](http://www.cameralibre.cc/wp-content/uploads/02_inside_government.0259-300x169.png)

the official scans a document, and it is digitally published on the screen next to the desk.

![torch](http://www.cameralibre.cc/wp-content/uploads/torch-300x169.jpg)

a hand enters the screen from below and turns on a torch, illuminating other documents in the room. More beams of light uncover a lot more information.
  
![information](http://www.cameralibre.cc/wp-content/uploads/inside_government-300x169.jpg)

## Changes for international contexts:  

- FOI Act/Paper/Informationsfreiheitsgesetz: change to Freedom of Information Act in local language

- official/flatmonitor/INFO/info: Translate INFO to other languages if necessary


