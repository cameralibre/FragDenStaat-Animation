## About

*'Freedom of Information & Frag den Staat'* is a vector animation created using the Free Software tools [Synfig Studio](synfig.org) and [Inkscape](inkscape.org).  
You can see the original video in its entirety at [Camera Libre](http://cameralibre.cc/fragdenstaat).  
I made the original in 2014 - I've since released and documented the project files to allow other people in other countries to use and adapt the video.

This is the first prototype of a larger project - Open Source Animation for Activism, Education, and Civic Organisations - see the intro video here:

![Open Source Animation for Activism, Education & Civic Organisations](webm/OpenSourceAnimation.webm)

_'Freedom of Information & Frag den Staat'_ is designed to explain and promote the German Freedom of Information website [Frag den Staat](http://fragdenstaat.de) ("Ask the State") as well as the idea of [Freedom of Information](https://en.wikipedia.org/wiki/Freedom_of_information) more generally.  
Frag den Staat was set up by [Stefan Wehrmeyer](http://stefanwehrmeyer.com/) and [Open Knowledge Foundation Deutschland](http://okfn.de), and since 2015 has been led by [Arne Semsrott](https://twitter.com/arnesemsrott).  
Stefan also created [FROIDE](https://github.com/stefanw/froide), the software which FdS is built on.  
FROIDE and the similar software project [Alaveteli](http://alaveteli.org/) are the basis of a number of FOI websites around the world, who share a common goal, and a common approach: to strengthen transparency and democracy, by enabling citizens to exercise their right to information about public bodies.
Hopefully this animation can be adapted and improved to serve their purposes.  

Original (German) version of scene 3:  
![German version](https://media.giphy.com/media/3o7TKGVSMJg5MU6pcA/source.gif)

Adapted (British) version of scene 3:  
![British version](https://media.giphy.com/media/l0MYSh3Qa23ZyOXCM/source.gif)

A Hungarian version has been animated for the site [Ki Mit Tud](https://kimittud.org), which will be published in the coming days, once sound mix and feedback rounds have been completed.  
![Hungarian version](https://media.giphy.com/media/3ohzdDp0Pd9vQnQHbq/source.gif)


## Goals of this project:
- To develop a template for any FOI portal to quickly, easily and economically create their own explanation animation - tailored for their site, their culture and audience. Also to allow people to further update and improve their animation as time goes on.


- To experiment and develop best practices for collaborative 2D animation using free software, and provide a _practical_ example for open sourcing animation, as well as using free/libre open source tools to create animation.

- To develop a more efficient and flexible workflow. Right now, the documentation and arrangement of the project is very much set up for a user working with Inkscape and Synfig Studio with a graphical user interface - ideally in the future it could be possible to use a command line or simple web app to replace specific text or other elements, automating changes which currently have to be made manually by clicking through the GUI. 
In addition, this may eventually be suited to flexible in-browser display using the [Web Animations API](http://danielcwilson.com/blog/2015/07/animations-intro/) rather than animating in Synfig and rendering to an inflexible, heavy video file. The technology (and my own abilities with it) have a way to go, however.


- To improve the original video. There are a wide range of problems with the original which I would like to fix (in terms of timing, design, sound design as well as the general text and concept). However, animation is extremely time consuming and at some stage I simply had to say "OK, it's done" - I (literally) couldn't afford to keep tinkering. Through allowing others to take the project and improve it, or being commissioned to do so myself, I hope that changes and improvements can benefit all versions of the video. By reworking the animation for the hungarian version I've been able to improve a few key design and animation elements, as well as refactoring the projects to make it easier to adapt in the future.

## Project Overview
The project is split into 12 Synfig projects - every time there is a hard scene cut, rather than an animated transition, I use a new project. This means that some scenes are very short (eg '06 document handover') whereas others are very long and complex ('02 inside government' and '10 laptop'). Scene 05 and scene 07 are actually contained within one project.  

PROJECT LIST:  

![title](img/jpg/01_title-300x169.jpg)

01 title  

![inside government-hu](https://media.giphy.com/media/xUPGcnOXtqOjv9OmjK/source.gif)

02 inside government - Hungarian version

![post](img/jpg/post-300x169.jpg)

03 post  

![document types-hu](https://media.giphy.com/media/xUPGclNtvWzNkLmRfa/source.gif)

04 document types - Hungarian version   

![05 protestors](img/jpg/05_protestors-300x169.jpg)

![07 protestors](img/jpg/07_protestors-300x169.jpg)

05 07 protestors   

![document handover-hu](https://media.giphy.com/media/xUPGcz4jNKgqQGH72w/source.gif)

06 document handover  

![multiple offices](https://media.giphy.com/media/3og0IunfNVeQOPT7DW/source.gif)

08 multiple offices -Hungarian version  

![combining data](img/jpg/combining_data-300x169.jpg)

09 combining data  

![laptop](https://media.giphy.com/media/3og0IKIyzprNW414WI/source.gif)

10 laptop - Hungarian version  

![end title](img/jpg/endtitle-300x169.jpg)

11 end title

When you export PNG sequences from each of these projects into their respective folders in /png, you can then open the video editing project 'FragDenStaat.kdenlive' and combine them with a new voiceover. (You can use the [Audacity](http://www.audacityteam.org/) project 'FragDenStaat_Mix.aup' to mix your voiceover with the existing sound effects.


## Repository Structure

**[/fonts](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/fonts)**
A large part of adapting the project for international use is replacing German-language text with the local language, and changing the design/branding to suit a different FOI website. Text visible in any scene can be easily changed by opening the .sif file in text editor and using its 'Find & Replace' function. However, differing word lengths may cause layour issues, so you will likely need to manually re-align text layers in Synfig.
 
**[/notes](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/notes)**
Here I've noted the elements in each scene which (probably) need to be changed to adapt the video to a different language or country, and created some basic documentation on the structure and concept of each particular scene.

**[/png](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/png)**
The original Frag den Staat video included some bitmap files which were referenced in the animations, which was not ideal. These files have been removed or replaced with vector elements in the Hungarian (Ki Mit Tud) version of the video.

**[/script](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/script)**
Voiceover script and concept for the entire video. It will need to be translated for international use and I would like to improve it along the way! This folder also includes subtitles in English, German and Chinese for the video. (Thanks Arne!) 

**[/sif](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/sif)**
[Synfig file format](http://wiki.synfig.org/wiki/File_Formats)
The main animation files are [Synfig](http://www.synfig.org/) projects (.sif). There's one project for each scene in the video. 
Other than some linked texture files (removed in updated versions), the elements in the projects are all vector graphics, meaning they are simply mathematical expressions of points, lines, curves, shapes or polygons. 
This means that the projects are very flexible, lightweight*, and as the projects are text-based XML files, they lead themselves well to version control & scripting. 
To learn the basics of Synfig, I would recommend the official [Synfig Studio Training Course](https://gumroad.com/l/TkoN).  


**[/svg](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/svg)**
[Inkscape file format](http://wiki.inkscape.org/wiki/index.php/Frequently_asked_questions#Are_Inkscape.27s_SVG_documents_valid_SVG.3F)  
The illustrations for this video were created in [Inkscape](http://inkscape.org)  - mostly by me, but with many contributions from [Judith Carnaby](http://judithcarnaby.com).
Personally, I find Inkscape more flexible and intuitive for creating vector images than Synfig, so I create my images in Inkscape and then save a copy as a .sif:  
(File>Save As... Synfig Animation *.sif)  

**[/textures](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/textures)**
The original version of the video included a number of public domain texture images (.jpg), which are overlaid on objects in Synfig. A texture can be imported (File>Import...) and placed directly above the groups or layers that you want to apply texture to. The 'Blending Mode' of the texture layer should be set to Overlay, and the 'Amount' (opacity) to something low, like 0,10.
The texture files are only _linked_ to the Synfig project, not embedded within the project. These textures are not used in the Hungarian version of the video.

**[/webm](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/webm)**
Reference videos for each scene. 

## License

Frag den Staat is currently licensed under [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/). 

I'm not sure if this actually makes sense for project files, or just the cultural content - if a software license is more appropriate, please let me know!

## FOI portals around the world:
(let me know if I've missed any)

Australia  
Right To Know  
https://www.righttoknow.org.au/

Austria  
Frag Den Staat  
https://fragdenstaat.at

Bosnia  
Pravo Da Znam  
http://www.pravodaznam.ba/

Brazil  
Queremos Saber   
http://queremossaber.org.br/

Canada  
Je Veux Savoir  (site is gone - perhaps the project is dead now?)
http://www.jeveuxsavoir.org/

Croatia  
Imamo pravo znati   
http://imamopravoznati.org/

Czech Republic  
Info Pro Všechny  
http://www.infoprovsechny.cz/

Europe  
Ask the EU  
http://www.asktheeu.org/

Germany  
Frag Den Staat  
https://fragdenstaat.de

Hong Kong  
accessinfo.hk  
https://accessinfo.hk/

Hungary  
Ki Mit Tud  
http://kimittud.atlatszo.hu/

Israel  
Ask Data  
http://askdata.org.il/

Italy  
Diritto Di Sapere  
https://chiedi.dirittodisapere.it/

Macedonia  
Слободен пристап  
http://www.slobodenpristap.mk/

New Zealand  
FYI  
https://fyi.org.nz/

Romania  
Nu Vă Supărați  
http://nuvasuparati.info/

Russian Federation
РосОтвет
http://rosotvet.ru/

Rwanda  
Sobanukirwa  
https://sobanukirwa.rw/

Serbia  
Da Zanamo Svi  
http://daznamosvi.rs/sr

South Africa
Right2Know
http://www.r2k.org.za/

Spain  
Tu Derecho A Saber  
http://tuderechoasaber.es/ 
Tu Derecho a Saber was shut down in December 2015 due to the government's refusal to respond to FOI requests submitted electronically!

Sweden
Fråga Staten
http://fragastaten.se/

Tunisia  
Marsoum 41  
http://www.marsoum41.org/en

Uganda
Ask Your Government
http://askyourgov.ug/

Ukraine  
Доступ до правди  
https://dostup.pravda.com.ua/

United Kingdom  
WhatDoTheyKnow  
https://www.whatdotheyknow.com/

United States
iFOIA
https://www.ifoia.org

Uruguay  
Que Sabes  
http://www.quesabes.org/
