ENTERING GOVERNMENT BUILDING:  
With the Freedom of Information Act, citizens can receive information from authorities - these authorities publish a lot of important information, but by no means everything!

SWITCHING ON TORCHES IN GOVERNMENT BUILDING:  
the Freedom of Information Act allows us to look into unpublished files and documents - when you use the FOI, you make the work of officials more public and transparent.

EXCHANGE OF LETTERS:  
any person may ask for information without giving a particular reason - the authorities have to answer them.

FALLING DOCUMENTS:  
through the FOI you can request various types of documents - for example building plans, correspondence, data sets, or internal meeting records.

PROTESTORS:  
without information it can sometimes be difficult to be heard.
with a Freedom of Information request, you can support your cause with evidence

VISITING OFFICES:  
information distributed throughout different parts of public administration can be combined to uncover interesting stories.

LAPTOP:  
for all these cases there is FragDenStaat.de

the site makes it easy to submit FOI requests.

GOVERNMENT DEPARTMENTS, LOCAL / STATE / FEDERAL:  
First you select the authority who might have the information you require.

REQUESTING INFORMATION ON NEW AIRPORT:  
in the next step, you briefly describe which informaiton you specifically require.

we package your application in a preformatted FOI request and send it to the authority on your behalf.

PUBLISHED FOI REQUEST + LAPTOPS:  
your request, together with the answer from the authority, will be published on FragDenStaat.de - this way, you can share the information with other interested parties, and drive attention to your cause or concern.

FragDenStaat is a free, nonprofit service from the Open Knoweledge Foundation Deutschland. 

LOGO:  
Help to shine light into public administration – _Frag den Staat!_ (ask the state)
