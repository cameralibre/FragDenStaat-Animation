These are the audio files used in my original mix for the FragDenStaat.de video. They should link up properly with little difficulty the when you open the video project with Kdenlive and choose 'search recursively'.

The audio files in this folder are all licensed either CC-BY or CC0 to their individual copyright holders, so it's important that attribution is included in your video and the info section of the site you choose to host it on (eg Youtube, Vimeo, Mediagoblin etc).
In my case, the list of sound effects were simply too long to include in the credits of the video - nobody would ever read them all - instead I included the following text in the video itself: 

**MUSIC:** 
XXV CC-BY Broke for Free 
**AUDIO FX:**
CC0 & CC-BY from FreeSound.org
Attribution & links: cameralibre.cc/fragdenstaat


On the linked site [cameralibre.cc/fragdenstaat](http://cameralibre.cc/fragdenstaat) you will find the full attribution with links & information, as follows:


Music:

*[XXV](http://brokeforfree.bandcamp.com/track/xxv)*
[CC-BY](https://creativecommons.org/licenses/by/4.0/) [Broke For Free](http://brokeforfree.com/)

[@brokeforfree](https://twitter.com/BrokeForFree) on Twitter


Sound Effects:

*The following sound effects can be found on [FreeSound.org](http://freesound.org/), a community of audio enthusiasts who create, share and remix sounds - the majority of these sounds are available under libre licenses such as Creative Commons Attribution, Attribution ShareAlike or the 'no rights reserved' public domain dedication,
CC0. Check out the artists' other sounds, or consider contributing yourself!*

**Sounds used under [Creative Commons
Attribution](https://creativecommons.org/licenses/by/4.0/):**

[letter-box.flac](http://www.freesound.org/people/qubodup/sounds/216571)
CC-BY qubodup

[peugot-206-car-exterior-engine-stationary-horn-claxon-reverb.wav](http://www.freesound.org/people/jorickhoofd/sounds/176638)
CC-BY jorickhoofd

[spring-door-stop.wav](http://www.freesound.org/people/liamq/sounds/170359)
CC-BY liamq

[deep-air-woosh.wav](http://www.freesound.org/people/cosmicembers/sounds/160757)
CC-BY cosmicmembers

[whoosh.wav](http://www.freesound.org/people/ztrees1/sounds/134935/)
CC-BY ztrees1

[envelope-handling-rustle-vtkproductions.wav](http://www.freesound.org/people/vtkproductions.com/sounds/131559)
CC-BY vtkproductions-com

[chev-350-start-then-die.wav](http://www.freesound.org/people/lonemonk/sounds/123686)
CC-BY lonemonk

[flashlight-switch.wav](http://www.freesound.org/people/jesabat/sounds/119727)
CC-BY jesabat

[stickwhoosh.wav](http://www.freesound.org/people/plingativator/sounds/112155)
CC-BY plingativator

[vent-wind-1.wav](http://www.freesound.org/people/Glaneur%20de%20sons/sounds/104952)
CC-BY glaneur-de-sons

[writing-with-pen.aif](http://www.freesound.org/people/jasonelrod/sounds/85484)
CC-BY jasonelrod

[clock-fastticking.wav](http://www.freesound.org/people/firefreak/sounds/47244)
CC-BY firefreak

[scanner-5.wav](http://www.freesound.org/people/THE_bizniss/sounds/39317)
CC-BY the-bizness

[woosh-02.wav](http://www.freesound.org/people/Glaneur%20de%20sons/sounds/34172)
CC-BY glaneur-de-sons

[t-driveby-backin.mp3](http://www.freesound.org/people/roscoetoon/sounds/26831)
CC-BY roscoetoon

**Sounds used under [Creative Commons
Zero](http://creativecommons.org/publicdomain/zero/1.0/) :**

[mouse-clicks.wav](http://www.freesound.org/people/joebro10/sounds/219318)
CC0 joebro10

[swish5.wav](http://www.freesound.org/people/alienxxx/sounds/218273)
CC0 alienxxx

[rolling-office-chair.wav](http://www.freesound.org/people/alpanaytekin/sounds/213086)
CC0 alpanaytekin

[woosh slide-in
2(longer).wav](http://www.freesound.org/people/xdwx/sounds/212957) CC0
xdwx

[letter-box.mp3](http://www.freesound.org/people/Taira%20Komori/sounds/212085)
CC-BY taira komori

[demo-berlin.flac](http://www.freesound.org/people/nikitralala/sounds/204756)
CC0 nikitralala

[wien-demo.wav](http://www.freesound.org/people/nikitralala/sounds/204474)
CC0 nikitralala

[worn-engine-revving.flac](http://www.freesound.org/people/kevaaq/sounds/203963)
CC0 kevaaq

[engine-crank-and-start.wav](http://www.freesound.org/people/chazznf/sounds/203162)
CC0 chazznf

[female-voice-cartoon-huh-question-aham.aiff](http://www.freesound.org/people/andrutzab/sounds/202880)
CC0 andrutzab

[letterbox-clang.mp3](http://www.freesound.org/people/jackdeb/sounds/198273)
CC0 jackdeb

[engine-subaru-forester-1989.wav](http://www.freesound.org/people/mario1298/sounds/181234)
CC0 mario1298

[sliding-doors.wav](http://www.freesound.org/people/swagmuffinplus/sounds/176146)
CC0 swagmuffinplus

[ding-ding-small-bell.wav](http://www.freesound.org/people/johnsonbrandediting/sounds/173932)
CC0 johnsonbrandediting

[backfire.ogg](http://www.freesound.org/people/ceebfrack/sounds/105351)
CC0 ceebfrack

[072510-distant-cheer.wav](http://www.freesound.org/people/sagetyrtle/sounds/102108)
CC0 sagetyrtle

[newspaper-9.wav](http://www.freesound.org/people/waterboy920/sounds/67040)
CC0 waterboy920

[three-passenger-jets-depart.wav](http://www.freesound.org/people/digifishmusic/sounds/54963)
CC0 digifishmusic

[sliding-paper-on-table.wav](http://www.freesound.org/people/123jorre456/sounds/46631)
CC0 123jorre456

[dropping-and-sliding-paper.wav](http://www.freesound.org/people/123jorre456/sounds/46625)
CC0 123jorre456

[paper.wav](http://www.freesound.org/people/stijn/sounds/43672) CC0
stijn

[switch-02.wav](https://freesound.org/people/VKProduktion/sounds/217494/)
CC0 vkproduktion